console.log('Hello World');

let x = 1

function createField(){


    var divElement = document.createElement('div');
    divElement.setAttribute('class', 'form-group');
    

    // var labelElement = document.createElement('label');
    // labelElement.setAttribute('for', 'places');
    // labelElement.innerHTML = `Place ${x++}`
    var newField = document.createElement('input');
    newField.setAttribute('type', 'text');
    newField.setAttribute('name', 'places');
    newField.setAttribute('placeholder', 'Enter a place');
    newField.setAttribute('class', 'form-control');
    // divElement.appendChild(labelElement);
    divElement.appendChild(newField);
    survey_options.appendChild(divElement);

}


function createPlaceTimeField(){


  // Get the first select tag
  const firstSelectTag = document.getElementById('placesID');

  // Get the options from the first select tag
  const options = Array.from(firstSelectTag.children).map(option => ({
    value: option.value,
    label: option.textContent
  }));


  // Create a new select tag with the same options
  const newSelectTag = document.createElement('select');
  newSelectTag.setAttribute('name','places')
  newSelectTag.setAttribute('class','form-control form-control-sm')

  options.forEach(option => {
    const newOption = document.createElement('option');
    newOption.value = option.value;
    newOption.textContent = option.label;
    newSelectTag.appendChild(newOption);
  });


  var rowElement = document.createElement('div')
  rowElement.setAttribute('class','row mb-3')

  var colElement1 = document.createElement('div')
  colElement1.setAttribute('class','col-6')

  colElement1.appendChild(newSelectTag)

  rowElement.appendChild(colElement1)


  var colElement2 = document.createElement('div')
  colElement2.setAttribute('class','col-6')

  timeElement = document.createElement('input')
  timeElement.setAttribute('type','time')
//   timeElement.setAttribute('value','06:00')
  timeElement.setAttribute('name','timefield')
  timeElement.required = true
  


  colElement2.appendChild(timeElement)


  rowElement.appendChild(colElement2)

  // Append the new select tag to the document
  const container = document.getElementById('places_fields');
  container.appendChild(rowElement)

  


}


