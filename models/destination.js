
const mongoose = require('mongoose')

const destinationSchema = new mongoose.Schema({


    startingPlace: {
        type: String,
        default:null

        },

    endingPlace:{
        type:String,
        // unique:true
    },

    combinedName: {
        type: String,
    },

    inbetweenPlaces: {
        type: Array,
        default: []
    }

    
})

module.exports = mongoose.model('destination',destinationSchema)