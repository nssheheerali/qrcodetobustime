
const mongoose = require('mongoose')

const busSchema = new mongoose.Schema({


    busName: {
        type: String,
        default:null
    },

    busNumber:{
        type:String,
        // unique:true
    },

    busRoute: {
        type: String,
    },
    


    inbetweenPlaces: {
        type: Array,
        default: []
    }

    
})

module.exports = mongoose.model('busdetail',busSchema)