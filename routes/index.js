var express = require('express');
var router = express.Router();
var QRCode = require('qrcode')
const mongoose = require('mongoose')


const Destination = require('../models/destination')
const BusModel = require('../models/bus')
const BusStop = require('../models/busstop')
const BusScheduleModel = require('../models/busSchedule')


router.get('/', async function(req, res, next) {
  const result =  await Destination.find().lean()
  res.render('index', {busRoutes: result});
});

router.get('/places',async function(req,res,next){
  const result = await BusStop.find().lean()
  res.render('pages/stops',{busStops:result})
})

router.get('/places/:placeid', async function(req, res, next) {
  const placeid = req.params.placeid

  try {
    
  // const result = await BusScheduleModel.find({busStops:{$elemMatch:{place:placeid}}},{_id:0,busStops:1})
  const result = await BusScheduleModel.aggregate([
    // Match documents that have at least one busStop with the matching placeid
    { $match: { 'busStops.place': placeid } },
    // Filter the busStops array to only include the matching elements
    {
      $project: {
         busName:1,
        _id:0,
        busStops: {
          $filter: {
            input: '$busStops',
            cond: { $eq: ['$$this.place', placeid] }
          }
        }
      }
    },
    // Unwind the busStops array to return one document per busStop
    { $unwind: '$busStops' },
    // Project the busStops array back to its original form
    // { $replaceRoot: { newRoot: '$busStops' } }
    {
      $lookup: {
        from: 'busdetails', // The name of the collection to join
        localField: 'busName',
        foreignField: '_id',
        as: 'busNameInfo'
      }
    },
    // Unwind the busNameInfo array to return one document per name
    { $unwind: '$busNameInfo' },
    // Project the output to include only the desired fields
    {
      $project: {
        _id: 0,
        busStops: 1,
        busName: '$busNameInfo.busName',
        busRoutes: '$busNameInfo.busRoute',
      }
    },
    // {
    //   $lookup:{
    //     from: 'destinations',
    //     localField: 'busRoutes',
    //     foreignField: '_id',
    //     as: 'busRouteInfo'
    //   }
    // },
    // { $unwind: '$busRouteInfo' },
    // {
    //   $project: {
    //     _id: 0,
    //     busStops: 1,
    //     busName: 1,
    //     busRoute: '$busRouteInfo.combinedName',
    //   }
    // }
  ])

  const busDirectionsID = [...new Set(result.map((val) => {
    return val.busRoutes;
  }))];

  console.log(busDirectionsID);


  const result2 = await BusStop.findById(placeid,{place:1,_id:0}).lean()
  const result3 = await Destination.find({ _id: { $in: busDirectionsID } }).lean();
  // console.log(result3);
  // console.log(result2);

  console.log(result);

  const combinedResult = result.map((val,index)=>{
    const direction = result3.find((val2)=>{
      return val2._id.toString() === val.busRoutes.toString()
    })
    return {
      busroute: val.busRoutes,
      busname: val.busName,
      bustime: val.busStops.time,
      direction: direction.combinedName
    }
  })

  console.log(combinedResult);

  const fullUrl = req.protocol + '://' + req.hostname + req.originalUrl;
  console.log(fullUrl);
  const url = await QRCode.toDataURL(fullUrl,{version:4296})


  res.render('pages/bus-stops',{place:result2.place,result:combinedResult,url:url})
  } catch (error) {
    console.log(error.message);
    res.render('pages/bus-stops',{error:"invalid place"})
  }

});


router.get('/add-bus-route', function(req, res, next) {
  res.render('pages/add-bus-route', { title: 'Test' });
});

router.post('/add-bus-route', function(req, res, next) {
  const {start,end,places } =  req.body
  console.log(req.body);
  
  console.log(start,end);
  const destination = new Destination({
    startingPlace: start,
    endingPlace: end,
    combinedName: start +"-"+ end,
    inbetweenPlaces: places

  })
  destination.save()
  .then(result => {
    console.log(result);
    res.redirect('/add-bus-route')
  })
  .catch(err => {
    console.log(err);
  })
});

router.get('/single-route/:id', async function(req, res, next) {
  const result = await Destination.findById(req.params.id).lean()
  res.render('pages/single-route', { title: 'Test', singleRoute: result });
});

router.get('/add-bus',async (req,res) => {
  
  const result = await Destination.find().lean()
  res.render('pages/add-bus', {busRoutes: result})
})

router.post('/add-bus',async (req,res) => {
  // console.log(req.body);



  const result = await new BusModel({
    busName: req.body.busName,
    busNumber: req.body.busNumber,
    busRoute: req.body.busRoute,
    // inbetweenPlaces: req.body.places
  })
  result.save()
  .then(result => {
    console.log(result);
    res.redirect('/add-bus')
  })
  .catch(err => {
    console.log(err);
  })

})

router.get('/qr-code',(req,res) => {
  const fullUrl = req.protocol + '://' + req.hostname + req.originalUrl;
  console.log(fullUrl);
  QRCode.toDataURL(fullUrl,{version:4296}, function (err, url) {
    console.log(url)
    res.render('pages/qrcode',{url})
  })
})


router.get('/add-stops',(req,res) => {
  res.render('pages/places')
})

router.post('/add-stops',(req,res) => {
  // console.log(req.body);
  
  const { places } = req.body

  const objects = places.map((place)=>{
    return { place:place }
  })

  BusStop.insertMany(objects).then((val)=>{
    console.log('Inserted Documents : ',val);
  }).catch((err)=>{
    console.log("Error Inserting Document",err.message)
  })
  res.render('pages/places')
})


router.get('/add-bus-time',async (req,res)=>{
    let result = await BusModel.find().lean()
    // console.log(result);


     result = result.map((obj)=>{
      return {
        _id:obj._id.toString(),
        busName: `${obj.busName}--${obj.busNumber}`

      }
    })
    console.log(result);

    const stops = await BusStop.find().lean()
    
    res.render('pages/bus-time',{buses:result,stops})
})

router.post('/add-bus-time',async (req,res)=>{
  console.log(req.body);
  let {busRoute,places,timefield } = req.body

  // if(places.length !== timefield.length){
  //   res.redirect('/add-bus-time')
  // }

  // if(places.length === 0){
  //   res.redirect('/add-bus-time')
  // }

  // if(timefield.length === 0){
  //   res.redirect('/add-bus-time')
  // }

  if(typeof places === 'string'){
    places = [places]
    timefield = [timefield]
  }





  const mergedArray = places.map((place, index) => {
    return {
      place: place,
      time: timefield[index]
    };
  });
  const result = new BusScheduleModel({
      busName:mongoose.Types.ObjectId(busRoute),
      busStops:mergedArray
  })
  result.save()
  res.redirect('/add-bus-time')
})


module.exports = router;
