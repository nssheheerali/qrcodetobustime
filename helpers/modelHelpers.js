const BusStop = require('../models/busstop')

module.exports = {

    getBusStops: async (id) => {
        return result = await BusStop.aggregate(
            [
                {
                  $match: {
                    _id: ObjectId(id),
                  },
                },
                {
                  $lookup: {
                    from: "busschedules",
                    localField: id,
                    foreignField: "bustops.place",
                    as: "result",
                  },
                },
                {
                  $unwind: "$result",
                },
                {
                  $unwind: "$result.busStops",
                },
              ]
        )

        console.log(result);
        // return result

    }


}